-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: Domaci_Borko
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Kupac`
--

DROP TABLE IF EXISTS `Kupac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kupac` (
  `Ime` varchar(40) COLLATE utf8_bin NOT NULL,
  `Prezime` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `Adresa` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kupac`
--

LOCK TABLES `Kupac` WRITE;
/*!40000 ALTER TABLE `Kupac` DISABLE KEYS */;
INSERT INTO `Kupac` VALUES ('Marko','Markovic','Bulevar Oslobodjenja 6',1),('Nikola','Milanovic','Koste Racina 18',2),('Sreten','Budilic','Kralja Petra 2',3);
/*!40000 ALTER TABLE `Kupac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Narudzbenica`
--

DROP TABLE IF EXISTS `Narudzbenica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Narudzbenica` (
  `Datum` date NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Narudzbenica`
--

LOCK TABLES `Narudzbenica` WRITE;
/*!40000 ALTER TABLE `Narudzbenica` DISABLE KEYS */;
INSERT INTO `Narudzbenica` VALUES ('2022-04-14',1),('2022-06-24',2),('2022-06-30',3);
/*!40000 ALTER TABLE `Narudzbenica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proizvod`
--

DROP TABLE IF EXISTS `Proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Proizvod` (
  `Naziv` varchar(50) COLLATE utf8_bin NOT NULL,
  `Barkod` varchar(13) CHARACTER SET utf8 DEFAULT NULL,
  `Cena` decimal(2,0) DEFAULT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proizvod`
--

LOCK TABLES `Proizvod` WRITE;
/*!40000 ALTER TABLE `Proizvod` DISABLE KEYS */;
INSERT INTO `Proizvod` VALUES ('Automatski osigurac 16A','asdfg',50,1),('Sklopka 25A  3P','qwerty',65,2),('Kompaktni prekidac 200A','zxcvb',99,3);
/*!40000 ALTER TABLE `Proizvod` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-15 15:34:21
